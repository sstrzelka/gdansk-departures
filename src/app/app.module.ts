import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule }   from '@angular/router';
import {JsonpModule} from '@angular/http';
import { AgmCoreModule } from 'angular2-google-maps/core';


import { AppComponent }  from './app.component';
import { FlightService } from './services/flight.service';
import { FlightsComponent } from './flights.component';
import {FlightDetailComponent} from './flight-detail.component'
import {DashboardComponent} from './dashboard.component'


@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule,JsonpModule,
  RouterModule.forRoot([
  {
    path: 'flights',
    component: FlightsComponent
  },
  {
    path: ':user',
    component: DashboardComponent
  },
  {
    path: 'successful',
    component: DashboardComponent
  },
  {
    path : 'detail/:id',
    component: FlightDetailComponent
  }
]) ],
  declarations: [ AppComponent,FlightsComponent, DashboardComponent, FlightDetailComponent],
  bootstrap:    [ AppComponent],
})
export class AppModule { }
