import { Component,Directive,Input } from '@angular/core';
import {FlightService} from './services/flight.service';
import { Router } from '@angular/router';
import {plainToClass} from "class-transformer";


import {Flight} from './Flight';
import { Airport } from './Airport'

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css'],
  
})
export class FlightsComponent  { 
  flights : Flight[];
  airports : Airport[];
  selectedFlight : Flight;  
  constructor(
      private flightService: FlightService,
      private router: Router,
){}  

  showFlights(){
    this.flights = this.flightService.getFlights();
  }
 
  onSelect(flight: Flight): void {
    this.selectedFlight = flight;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedFlight.flightId]);
  }
}

