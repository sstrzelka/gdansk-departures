export class Flight{
    flightId : string;
    carrierFsCode : string;
    flightNumber : string;
    departureAirportFsCode : string;
    arrivalAirportFsCode : string;
    departureDate:{
        dateLocal: Date
        dateUtc : string;
    }
    arrivalDate:{
        dateLocal : Date;
        dateUtc : string;
    }
    flightEquipment : {
        scheduledEquipmentIataCode : string,
    }
    city : string;
    getCarrierFsCode(): string{
        return this.carrierFsCode;
    }
}