export class Airport{
    fs : string;
    city: string;
    countryName : string;
    latitude : number;
    longitude : number;
}