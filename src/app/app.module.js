"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var cookies_service_1 = require("angular2-cookie/services/cookies.service");
var http_2 = require("@angular/http");
var app_component_1 = require("./app.component");
var flights_component_1 = require("./flights.component");
var flight_detail_component_1 = require("./flight-detail.component");
var dashboard_component_1 = require("./dashboard.component");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, http_1.HttpModule, forms_1.FormsModule, http_2.JsonpModule,
                router_1.RouterModule.forRoot([
                    {
                        path: 'flights',
                        component: flights_component_1.FlightsComponent
                    },
                    {
                        path: ':user',
                        component: dashboard_component_1.DashboardComponent
                    },
                    {
                        path: 'successful',
                        component: dashboard_component_1.DashboardComponent
                    },
                    {
                        path: 'detail/:id',
                        component: flight_detail_component_1.FlightDetailComponent
                    }
                ])],
            declarations: [app_component_1.AppComponent, flights_component_1.FlightsComponent, dashboard_component_1.DashboardComponent, flight_detail_component_1.FlightDetailComponent],
            providers: [cookies_service_1.CookieService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map