 import 'rxjs/add/operator/switchMap';
import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import { Flight }         from './flight';
import {Airport}          from './Airport'
import { FlightService }  from './services/flight.service';

declare var google : any;
@Component({
  moduleId: module.id,
  selector: 'flight-detail',
  templateUrl: './flight-detail.component.html',
  styleUrls: ['./flights.component.css'],
})
export class FlightDetailComponent{
   flight : Flight;
   departureAirport : Airport;
   arrivalAirport : Airport;
   distance : number;
   duration : number;

  constructor(
    private flightService: FlightService,
    private route: ActivatedRoute,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.flight = this.flightService.getFlight(this.location.prepareExternalUrl(this.location.path()).substring(8));
    this.departureAirport = this.flightService.getAirports().find(airport => airport.fs === this.flight.departureAirportFsCode);
    this.arrivalAirport = this.flightService.getAirports().find(airport => airport.fs === this.flight.arrivalAirportFsCode);
    this.distance = google.maps.geometry.spherical
      .computeDistanceBetween (new google.maps.LatLng(this.departureAirport.latitude, this.departureAirport.longitude), new google.maps.LatLng(this.arrivalAirport.latitude, this.arrivalAirport.longitude));
    this.distance /= 1000;
    this.duration = (Date.parse(this.flight.arrivalDate.dateUtc) - Date.parse(this.flight.departureDate.dateUtc)) / (1000 * 60);
}

  goBack(): void {
    this.location.back();
  }
}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
