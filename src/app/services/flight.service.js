"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/observable/forkJoin");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
var http_2 = require("@angular/http");
var FlightService = (function () {
    function FlightService(http, _jsonp) {
        this.http = http;
        this._jsonp = _jsonp;
    }
    FlightService.prototype.init = function () {
        this.params = new http_2.URLSearchParams();
        this.params.set('format', 'json');
        this.params.set('callback', '__ng_jsonp__.__req0.finished');
        this.today = new Date();
        this.url = "https://api.flightstats.com/flex/flightstatus/rest/v2/jsonp/airport/status/GDN/dep/" + this.today.getFullYear() + '/' + (this.today.getMonth() + 1) + '/' + this.today.getDate() + '/' + this.today.getHours() + "?appId=c1aa6fe9&appKey=ea7c975e3fdbc0dff542474bd05a8faf&utc=false&numHours=6";
    };
    FlightService.prototype.getAirports = function () {
        this.init();
        return this._jsonp.request(this.url, { search: this.params }).
            map(function (res) { return res.json(); });
    };
    FlightService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, http_2.Jsonp])
    ], FlightService);
    return FlightService;
}());
exports.FlightService = FlightService;
//# sourceMappingURL=flight.service.js.map