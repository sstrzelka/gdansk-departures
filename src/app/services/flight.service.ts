import {Injectable, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Jsonp, URLSearchParams} from '@angular/http';
import {plainToClass} from "class-transformer";


import {Flight} from '../Flight';
import {Airport} from '../Airport';

@Injectable()
export class FlightService{
      params : any;
      today : Date;
      url : string;
      flights : Flight[];
      airports : Airport[];
    constructor(private http: Http, private _jsonp: Jsonp){
        this.getData();
    }
    init(){
        this.params = new URLSearchParams();
        this.params.set('format', 'json');
        this.params.set('callback', '__ng_jsonp__.__req0.finished');
        this.today = new Date();
        this.url = "https://api.flightstats.com/flex/flightstatus/rest/v2/jsonp/airport/status/GDN/dep/" + this.today.getFullYear() + '/' + (this.today.getMonth() + 1) + '/' + this.today.getDate() + '/' + this.today.getHours() + "?appId=c1aa6fe9&appKey=XXX&utc=false&numHours=6";
    }
    getData(){
        this.init();
        this._jsonp.request(this.url, {search: this.params }).
          map(res => res.json()).subscribe(data => {
            this.flights = plainToClass(Flight,data.flightStatuses);
            this.airports = plainToClass(Airport,data.appendix.airports);   
            this.flights.sort((a,b) => {
            if (a.departureDate.dateLocal > b.departureDate.dateLocal)
                return 1;
            return -1;
            });
            this.flights.map(flight => flight.city = this.airports.find(airport => airport.fs == flight.arrivalAirportFsCode).city);
            })
    }
    getFlights() : Flight[]{
       return this.flights;
    }
    getAirports() : Airport[]{
        return this.airports;
    }
    getFlight(id: string) : Flight{
        return this.flights.find(flight => flight.flightId == id);
    }
}
