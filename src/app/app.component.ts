import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router'
import {DashboardComponent} from './dashboard.component'
import {FlightService} from './services/flight.service'


@Component({
  selector: 'app-root',
  template: `
     <h1>{{title}}</h1>
     <a routerLink="/flights">Flights</a>
     <router-outlet></router-outlet>
   `,
   providers : [FlightService]
})
export class AppComponent  {
  title = 'Last minute flights'; 

  constructor(private activatedRoute: ActivatedRoute) { 
  }

  
}