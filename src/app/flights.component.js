"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var flight_service_1 = require("./services/flight.service");
var class_transformer_1 = require("class-transformer");
var Flight_1 = require("./Flight");
var Airport_1 = require("./Airport");
var FlightsComponent = (function () {
    function FlightsComponent(flightService) {
        this.flightService = flightService;
    }
    FlightsComponent.prototype.showFlights = function () {
        var _this = this;
        this.flightService.getAirports().subscribe(function (data) {
            _this.flights = class_transformer_1.plainToClass(Flight_1.Flight, data.flightStatuses);
            _this.airports = class_transformer_1.plainToClass(Airport_1.Airport, data.appendix.airports);
            _this.flights.sort(function (a, b) {
                if (a.departureDate.dateLocal > b.departureDate.dateLocal)
                    return 1;
                return -1;
            });
            _this.flights.map(function (flight) {
                flight.city = _this.airports.find(function (airport) { return airport.fs == flight.arrivalAirportFsCode; }).city;
            });
        });
    };
    FlightsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-app',
            templateUrl: './flights.component.html',
            styleUrls: ['./flights.component.css'],
            providers: [flight_service_1.FlightService]
        }),
        __metadata("design:paramtypes", [flight_service_1.FlightService])
    ], FlightsComponent);
    return FlightsComponent;
}());
exports.FlightsComponent = FlightsComponent;
//# sourceMappingURL=flights.component.js.map